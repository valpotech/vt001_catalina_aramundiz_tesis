# Configuración de Entorno

## Instalación de librerías necesarias para Python 3.

1. Libreria OpenCV: permite controlar webcam

	pip3 install opencv-python

2. Listar cámaras en el sistema (para conocer el índice en que se encuentra la cámara que se utilizará).

	v4l2-ctl -d /dev/video0 --list-formats