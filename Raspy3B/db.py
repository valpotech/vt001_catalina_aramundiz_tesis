#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pymysql


MACHINE = 8365792
connect_info = ("35.247.251.105","rpi1","Ras_1234pi","mercado_publico")
qr_data = 6760072125
card_data = '4349565200405813¿22102063759616900001'

def one_word(word):
	if ' ' not in word:
		return word
	else:
		splitted = word.split(" ")
		one_word = splitted[0]
	return one_word

def get_data(data_in):
	data = str(data_in)
	tipo_plastico = data[0]
	rut_empresa = data[1:]
	#print("Tipo de plástico: " + tipo_plastico)
	#print("Rut de la empresa: " + rut_empresa:-1] + "-" + rut_empresa[-1])
	return (int(tipo_plastico),rut_empresa)

def identify_user(card_data,connect_info):
	host,user,password,database = connect_info

	sql_select = "SELECT user_name FROM registered_users \
				WHERE user_id = '%s';" % \
				(str(card_data))

	db = pymysql.connect(host,user,password,database)
	cursor = db.cursor()
	try:
		cursor.execute(sql_select)
		result = cursor.fetchone()[0]
		#print("Listo!")
	except:
		#print("Error: " + cursor.fetchone())
		result = 'No Registrado'
	db.close()
	
	return result

def send_data(qr_data,card_data,connect_info):
	ptype,rut = get_data(qr_data)

	if(identify_user(card_data,connect_info) != 'No Registrado'):
		user_id = card_data
	else:
		user_id = 'No Registrado'

	host,user,password,database = connect_info

	sql_insert = "INSERT INTO rec_items(id_maquina,tipo_plastico,rut_empresa,user_id) \
				VALUES (%s,%s,%s,'%s');" % \
				(str(MACHINE),str(ptype),str(rut),user_id)
	#print(sql_insert)
	db = pymysql.connect(host,user,password,database)
	cursor = db.cursor()
	try:
		cursor.execute(sql_insert)
		db.commit()
		#print("Listo!")
	except:
		db.rollback()
		#print("Error: " + cursor.fetchone())
	db.close()
	
	return

#send_data(qr_data,card_data,connect_info)
#print(identify_user('6281569312095756¿2608501546390000000',connect_info))
