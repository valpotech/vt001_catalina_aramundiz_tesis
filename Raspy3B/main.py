#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from db import *
from lcd_i2c import *
from qr_scanner import *

def sixteen_char(string):
	l = len(string)
	if(l < 16):
		spaces = ''
		needed = 16 - l
		for i in range(needed):
			spaces += ' '
		new_string = string + spaces
	elif(l == 16):
		new_string = string
	else:
		new_string = string[:16]
	print(len(new_string))
	return new_string

# Informacion de conexion base de datos
#MACHINE = 8365792
#connect_info = ("35.247.251.105","rpi1","Ras_1234pi","mercado_publico")
# Initialise display
lcd_init()

state = 'INIT'

while(True):
	# Estado 1: Esperando que el usuario acerque un código QR a la cámara.
	if(state == 'INIT'):
		lcd_string("                ",LCD_LINE_1)
		lcd_string("Esperando QR... ",LCD_LINE_2)

		print("Esperando QR...")

		qr_data = qr_scanner()
		state = 'CARD'

	# Estado 2: Esperando que el usuario deslice una tarjeta asociada a su cuenta.
	#	Esto supone que todo usuario tiene una cuenta o puede deslizar cualquier tarjeta.
	elif(state == 'CARD'):
		lcd_string("Deslice tarjeta ",LCD_LINE_1)
		lcd_string("                ",LCD_LINE_2)

		print("Deslice tarjeta")
		user_id = input()
		state = 'CHECK'

	# Estado 3: Comprobación de la información ingresada
	elif(state == 'CHECK'):
		lcd_string("Comprobando ... ",LCD_LINE_1)
		lcd_string("                ",LCD_LINE_2)

		print("Comprobando ... ")
		qr_decoded = get_data(qr_data)
			
		send_data(qr_data,user_id,connect_info)

		user_name = one_word(identify_user(user_id,connect_info))

		state = 'PRINT'
	# Estado 4: mostrar resultado al usuario.
	elif(state == 'PRINT'):
		lcd_string(sixteen_char(user_name),LCD_LINE_1)
		lcd_string(sixteen_char("plastico: " + str(qr_decoded[0])),LCD_LINE_2)

		print(user_name)
		print("Plastico: "+str(qr_decoded[0]))
		time.sleep(3)
		state = 'END'

	elif(state == 'END'):
		lcd_string("Finalizado      ",LCD_LINE_1)
		lcd_string("       Gracias! ",LCD_LINE_2)
		
		time.sleep(2)
		lcd_string("                ",LCD_LINE_1)
		lcd_string("                ",LCD_LINE_2)
		time.sleep(0.5)
		state = 'INIT'
			

	else:
		state = 'INIT'


