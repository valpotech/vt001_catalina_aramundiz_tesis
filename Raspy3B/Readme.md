### Cargando Raspbian en Raspberry 3B

1. Descargar Etcher desde [aquí](https://www.balena.io/etcher/).
2. Descargar Raspbian desde [aquí](https://www.raspberrypi.org/downloads/raspbian/).
    * Versión actual: Buster
    * Se utilizará la versión con entorno gráfico (Buster with Desktop) para esta instalación.
3. Ejecutar Etcher.
    * Select image: seleccionar el archivo con el sistema operativo descargado desde la página de Raspberry.
    * Select disk: seleccionar disco de destino. 
        - Etcher detecta automáticamente el disco, pero de todas formas es bueno revisar que sea el disco que realmente utilizaremos.
    * Flash: instala el sistema operativo en el destino escogido.
        - A tener en cuenta: el proceso dura aproximadamente 7 minutos en total. utilizando un adaptador microSD a USB3.0 en un notebook sin grandes prestaciones.
    * __Nota: el manual de instalación oficial se encuentra en este [link](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).__
    
### Configuración Inicial Raspberry 3B y habilitación de SSH

1. Conectar monitor mediante cable HDMI, teclado y mouse vía USB.
2. Enchufar dispositivo (se encenderá automáticamente).
3. Seguir configuración inicial (en la bienvenida al sistema).
4. Abrir terminal y ejecutar `sudo raspi-config`.
5. Seleccionar `Interfacing Options`.
6. Seleccionar `SSH` y habilitar el servidor.
7. Listo, ahora es posible conectarte mediante otro computador en la misma red a tu Raspberry.
    * En linux, basta abrir un terminal y ejecutar `ssh pi@raspberrypi.local` 
    * Una vez aceptada la conexión e ingresada la contraseña, estarás dentro del sistema.

* Por seguridad, recuerda desactivar el servidor SSH de la placa cuando tu aplicación esté corriendo. Este puede ser un punto débil en seguridad y permite el ingreso de cualquier usuario que conozca la clave (o que use alguna herramienta que se salte la clave) el ingreso a tu sistema, aplicación, red y otros.

### Configuración e Instalación librerías.
Basado en: https://www.pyimagesearch.com/2018/05/21/an-opencv-barcode-and-qr-code-scanner-with-zbar/

1. Fundamental: actualizar sistema

            sudo apt autoremove
            sudo apt update
            sudo apt upgrade
        
2. Se requieren algunos paquetes de sistema previos

            sudo apt install libhdf5-dev libhdf5-serial-dev libhdf5-103
            sudo apt install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5
            sudo apt install libatlas-base-dev
            sudo apt install libjasper-dev

3. Instalar (o actualizar pip en Raspberry)

            wget https://bootstrap.pypa.io/get-pip.py
            sudo python3 get-pip.py

4. Zbar viene preinstalado.
    * Si no está instalado, ejecutar el sgte comando

            sudo apt install libzbar0

5. Instalar PyZbar (zbar para python)

            sudo pip install pyzbar

6. Instalar paquete OpenCV para Python3
            
            sudo apt install python3-opencv

7. Instalar paquete imutils

            sudo pip install imutils

