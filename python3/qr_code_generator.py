import pyqrcode
import png
import time

################
## info_to_code: list with important parameters to code.
## img_format:   format for saving image. Default: png.
## img_name:     name for the image. Default: exact time of creation.
## show:         show image on screen or not. Default: no.
################
def create_qr(info_to_code,img_format = 'png',img_size = 1,img_name = time.time(),show = 'no'):
	to_code = ''
	for value in info_to_code:
		to_code += str(value).upper()

	qr_code = pyqrcode.create(str(to_code), error='H', version=1, mode='alphanumeric')

	if(img_format == 'svg'):
		qr_code.svg('qr/'+str(img_name)+'.svg', scale=img_size)
	elif(img_format == 'eps'):
		qr_code.eps('qr/'+str(img_name)+'.eps', scale=img_size)
	else:
		qr_code.png('qr/'+str(img_name)+'.png', scale=img_size, module_color=[0, 0, 0, 128], background=[0xff, 0xff, 0xff])

	if(show in ['yes','si',1]):
		print(qr_code.terminal(quiet_zone=1))

	return qr_code

example_ruts = [760072125,761436368,763373711,907030008,967148709]


for rut in example_ruts: # Use the ruts
	for i in range(1,8): # Plastic from 1 to 7
		qr_text = str(i) + str(rut)
		size = 2
		code = create_qr(qr_text,img_format = 'svg',img_size = size,img_name = qr_text+'_'+str(size),show = 0)
		print("Código creado: " + qr_text)
		#print(code.get_png_size())
		#print(code.text())

